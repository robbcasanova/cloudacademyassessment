const path = require('path')

module.exports = {
  verbose: true,
  transform: {
    '^.+\\.[t|j]sx?$': 'babel-jest',
  },
  transformIgnorePatterns: [],
  testPathIgnorePatterns: ['/node_modules/', '.history'],
}
