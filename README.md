# Cloud Academy Assessment

## Install

To install dependencies use
`npm ci`

## Run

Run exercises with `npm start`

## Tests

Run tests with `npm test`
