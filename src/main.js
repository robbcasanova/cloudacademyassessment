import data from '@/mocks/data'
import {
  getScorePoints,
  filterScorePointsByDateInterval,
  getScorePointsWithExtraInfo,
} from '@/utils/score'
import { getExtraPoints } from '@/utils/extra'

/**
 * Exercise 1:
 * Write a function that having a start_date and an end_date as input returns the subset of data included between the two for the slug ‘aggregation-overall’ and for the key ‘score’
 * Assume the start_date and end_date exactly match the “x” key in the serie
 * start_date and end_date must be included in the returned data.
 * The series always contains start_date and end_date
 */
;(function ex1(startDate, endDate) {
  const scorePoints = getScorePoints(data)
  const response = filterScorePointsByDateInterval(
    scorePoints,
    startDate,
    endDate,
    { exactMatch: true }
  )
  console.log(
    '\x1b[36m%s\x1b[0m',
    `Get score points between ${startDate} and ${endDate}: `
  )
  console.log(response)
})('2015-08-19T14:00:19.352000Z', '2015-10-12T07:27:47.493000Z')

/**
 * Exercise 2:
 * Write the same function as above to match the case that:
 * The series does not always contains end_date or start_date
 * Start_date and end_date don’t match the “x” key in the serie
 */
;(function ex2(startDate, endDate) {
  const scorePoints = getScorePoints(data)
  debugger
  const response = filterScorePointsByDateInterval(
    scorePoints,
    startDate,
    endDate
  )
  console.log(
    '\x1b[36m%s\x1b[0m',
    `Get score points between ${startDate} and ${endDate}: `
  )
  console.log(response)
})('2019-08-01T00:00:00.000Z', '2019-08-31T00:00:00.000Z')

/**
 * Exercise 3:
 * Consider that we want to display the data with the key “extra” on mouse over on a point of the key “score”. Write a function to format the data for this use case.
 */
;(function ex3(date) {
  const scorePoints = getScorePoints(data)
  const extraPoints = getExtraPoints(data)

  const scorePointsWithExtra = getScorePointsWithExtraInfo(
    scorePoints,
    extraPoints
  )

  console.log(
    '\x1b[36m%s\x1b[0m',
    `Get score point with extra data for ${date}`
  )

  console.log(scorePointsWithExtra.find((point) => point.x === date))
})('2015-08-19T14:00:19.352000Z')
