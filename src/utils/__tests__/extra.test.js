import { getExtraPoints } from '../extra'
import extraPoints from './extraPoints.json'
import aggregationOverallPayload from '@/mocks/data.json'

describe('utils - score', () => {
  describe('getScorePoints', () => {
    it('should return the list of extra points ({x: date, y: extra info}) given an aggregation-overall payload', () => {
      const res = getExtraPoints(aggregationOverallPayload)
      expect(res).toStrictEqual(extraPoints)
    })
  })
})
