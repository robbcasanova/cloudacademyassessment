import {
  getScorePoints,
  filterScorePointsByDateInterval,
  getScorePointsWithExtraInfo,
} from '../score'
import scorePoints from './scorePoints.json'
import extraPoints from './extraPoints.json'
import aggregationOverallPayload from '@/mocks/data.json'

describe('utils - score', () => {
  describe('getScorePoints', () => {
    it('should return the list of score points ({x: date, y: score}) given an aggregation-overall payload', () => {
      const res = getScorePoints(aggregationOverallPayload)
      expect(res).toStrictEqual(scorePoints)
    })
  })

  describe('filterScorePointsByDateInterval', () => {
    describe('option - exact match:true', () => {
      it('should return the list of score points included between startDate and endDate given that startDate and endDate have an exact match in the points list', () => {
        const res = filterScorePointsByDateInterval(
          scorePoints,
          '2015-12-30T15:24:21.361000Z',
          '2017-01-17T12:51:49.535573Z',
          { exactMatch: true }
        )
        expect(res).toStrictEqual([
          {
            y: 149,
            x: '2015-12-30T15:24:21.361000Z',
          },
          {
            y: 149,
            x: '2016-01-08T12:10:57.057000Z',
          },
          {
            y: 139,
            x: '2016-05-10T14:34:41.978000Z',
          },
          {
            y: 135,
            x: '2016-10-07T12:29:37.906829Z',
          },
          {
            y: 89,
            x: '2017-01-17T12:49:44.592529Z',
          },
          {
            y: 82,
            x: '2017-01-17T12:51:49.535573Z',
          },
        ])
      })
    })
    describe('option - exact match:false', () => {
      it('should return the list of score points between two given dates', () => {
        const res1 = filterScorePointsByDateInterval(
          scorePoints,
          '2015-12-30T00:00:00.000Z',
          '2015-12-30T23:59:59.000Z'
        )
        expect(res1).toStrictEqual([
          {
            y: 144,
            x: '2015-12-30T13:24:38.456000Z',
          },
          {
            y: 149,
            x: '2015-12-30T13:27:08.053000Z',
          },
          {
            y: 149,
            x: '2015-12-30T15:05:53.792000Z',
          },
          {
            y: 149,
            x: '2015-12-30T15:24:21.361000Z',
          },
        ])

        const res2 = filterScorePointsByDateInterval(
          scorePoints,
          '2000-01-01T00:00:00.000Z',
          '2015-08-19T14:00:19.352000Z'
        )

        expect(res2).toStrictEqual([
          {
            y: 282,
            x: '2015-08-19T14:00:19.352000Z',
          },
        ])

        const res3 = filterScorePointsByDateInterval(
          scorePoints,
          '2019-11-19T00:00:00.000Z',
          '2030-01-01T00:00:00.000Z'
        )

        expect(res3).toStrictEqual([
          {
            y: 308,
            x: '2019-11-19T17:14:34.796982Z',
          },
        ])
      })
      it('should include startDate and endDate if they match exact points date in list', () => {
        const res = filterScorePointsByDateInterval(
          scorePoints,
          '2015-12-30T15:24:21.361000Z',
          '2017-01-17T12:51:49.535573Z'
        )
        expect(res).toStrictEqual([
          {
            y: 149,
            x: '2015-12-30T15:24:21.361000Z',
          },
          {
            y: 149,
            x: '2016-01-08T12:10:57.057000Z',
          },
          {
            y: 139,
            x: '2016-05-10T14:34:41.978000Z',
          },
          {
            y: 135,
            x: '2016-10-07T12:29:37.906829Z',
          },
          {
            y: 89,
            x: '2017-01-17T12:49:44.592529Z',
          },
          {
            y: 82,
            x: '2017-01-17T12:51:49.535573Z',
          },
        ])
      })
      it("should return [] if there's no score points in the interval", () => {
        const res = filterScorePointsByDateInterval(
          scorePoints,
          '2015-12-30T15:25:00.000Z',
          '2015-12-30T15:26:00.000Z'
        )
        expect(res).toStrictEqual([])
      })
    })
  })

  describe('getScorePointsWithExtraInfo', () => {
    it('should return [] if scorePoints is empty', () => {
      const res = getScorePointsWithExtraInfo([], extraPoints)
      expect(res).toStrictEqual([])
    })
    it("should return the list of passed score points with an 'extra' key containing additional informations", () => {
      const res = getScorePointsWithExtraInfo(scorePoints, extraPoints)
      expect(res[0].extra).toBeDefined()
      expect(res[0]).toStrictEqual({
        y: 282,
        x: '2015-08-19T14:00:19.352000Z',
        extra: {
          quiz_session_type: 'Study',
          priority: 282,
          score_delta: null,
          quiz_session: 6775,
          quiz_config: 226,
          quiz_config_title: 'Platform Reference for AWS',
        },
      })
      expect(res[1].extra).toBeDefined()
      expect(res[1]).toStrictEqual({
        y: 227,
        x: '2015-10-08T14:45:31.991000Z',
        extra: {
          quiz_session_type: 'Study',
          priority: 55,
          score_delta: -55,
          quiz_session: 19037,
          quiz_config: 226,
          quiz_config_title: 'Platform Reference for AWS',
        },
      })
    })
  })
})
