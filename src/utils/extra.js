/**
 * Return the list of extra information points {x: date, y: extra info} given an aggregation-overall payload
 * @param {Object} aggregationOverallPayload
 */
export function getExtraPoints(aggregationOverallPayload) {
  const aggregationOverall = aggregationOverallPayload.data.find(
    (aggregatedData) => aggregatedData.slug === 'aggregation-overall'
  )
  const extra = aggregationOverall.details.find(
    (detail) => detail.key === 'extra'
  )

  return extra.series
}
