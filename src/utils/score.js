/**
 * Return the list of score points ({x: date, y: score}) given an aggregation-overall payload
 * @param {Object} aggregationOverallPayload
 */
export function getScorePoints(aggregationOverallPayload) {
  const aggregationOverall = aggregationOverallPayload.data.find(
    (aggregatedData) => aggregatedData.slug === 'aggregation-overall'
  )
  const score = aggregationOverall.details.find(
    (detail) => detail.key === 'score'
  )

  return score.series
}

/**
 * Return the list of score points between two given dates
 * @param {Array} scorePoints List of Score points in format {x: ISoDateString, y: score}
 * @param {ISO date string} startDate
 * @param {ISO date string} endDate
 * @param {} options
 * @param {boolean} options.exactMatch Set to true for fast filtering (require that startDate and endDate have an exact match in the Score points x value)
 */
export function filterScorePointsByDateInterval(
  scorePoints,
  startDate,
  endDate,
  options = {}
) {
  let filteredPoints = []

  // exactMatch is fast because use string comparison to filter the points
  if (options.exactMatch) {
    let pointInInterval = false
    scorePoints.forEach((point) => {
      if (point.x === startDate) {
        pointInInterval = true
      }
      if (pointInInterval) {
        filteredPoints.push(point)
      }
      if (point.x === endDate) {
        pointInInterval = false
      }
    })
  } else {
    const startDateObj = new Date(startDate)
    const endDateObj = new Date(endDate)

    filteredPoints = scorePoints.filter((point) => {
      const pointDateObj = new Date(point.x)
      return pointDateObj >= startDateObj && pointDateObj <= endDateObj
    })
  }

  return filteredPoints
}

/**
 * Return the list of passed score points with an 'extra' key containing additional informations
 * @param {Array} scorePoints
 * @param {Array} extraPoints
 */
export function getScorePointsWithExtraInfo(scorePoints, extraPoints) {
  if (!scorePoints.length) {
    return []
  }

  return scorePoints.map((point, idx) => ({
    ...point,
    extra: extraPoints[idx].y,
  }))
}
